#
#
#
DAEMON		= dateSiteDaemons
DEFDIR		= /etc/sysconfig
INITDIR		= /etc/init.d
UID		:= $(shell echo $$UID)
default:
	@echo "Choose target 'install' or 'uninstall'"  
	@echo "Must be executed as root user" 


check_uid:
	@if test $(UID) -ne 0 ; then \
		echo "Must be executed as root user" > /dev/stderr ; \
		false ; fi 

install: check_uid
	@echo "Remember to change date/.commonScripts/dateSiteDaemons to "
	@echo "NOT use SSH"
	cp $(DAEMON).sysconfig $(DEFDIR)/$(DAEMON)
	cp $(DAEMON).init      $(INITDIR)/$(DAEMON)
	/sbin/chkconfig --add $(DAEMON)
	/sbin/service   $(DAEMON) start
	/sbin/service   $(DAEMON) status


uninstall: check_uid	
	/sbin/service   $(DAEMON) status
	/sbin/service   $(DAEMON) stop
	/sbin/chkconfig --del $(DAEMON)
	rm -f $(INITDIR)/$(DAEMON)
	rm -f $(DEFDIR)/$(DAEMON)

status:
	/sbin/service   $(DAEMON) status

#
# EOF
#
